Description: direct changes to sources left from previous builds
Author: Thorsten Alteholz <debian@alteholz.de>
Reviewed-By: <name and email of someone who approved the patch>
Last-Update: 2017-02-19

--- bottlerocket-0.05b3.orig/br.c
+++ bottlerocket-0.05b3/br.c
@@ -288,6 +288,15 @@ int open_port(br_control_info *cinfo, ch
  */
     int fd;
 
+/*
+ * A slightly paranoid hack to make sure we are not outputting on stdout
+ * over a serial port accidentally. We prevent this by not opening 
+ * file descriptors 0, 1 or 2.
+ * added by Kevin Coyner, original code by A. Clark 15 Feb 2000
+ */
+    while ((fd = open("/dev/null", 0, 0)) < 3)
+        if (fd < 0) _exit(errno);
+    close(fd);
 
     if (Verbose >= 2)
         printf("%s: Opening serial port %s.\n", MyName, port);
@@ -305,11 +314,15 @@ int open_port(br_control_info *cinfo, ch
      * If we end up with a reserved fd, don't mess with it.  Just to make sure.
      */
     
-    if (!SAFE_FILENO(fd)) {
-        close(fd);
-        errno = EBADF;
-        return -1;
-    }
+/*
+ * Commented out by Kevin Coyner per patch from A.Clark 15 Feb 2000
+ *
+ *   if (!SAFE_FILENO(fd)) {
+ *       close(fd);
+ *       errno = EBADF;
+ *       return -1;
+ *   }
+*/
 
     return fd;
 }
--- /dev/null
+++ bottlerocket-0.05b3/rocket_launcher
@@ -0,0 +1,178 @@
+#!/usr/bin/wish
+
+#  Some variables to store desired states
+set bottleRocket "/usr/bin/br"
+set currentHouse "A"
+set currentButton 1
+set currentRange  0
+
+#  This is a tk/tcl front end for the bottlerocket program written by
+#Tymm ( tymm@acm.org )  It currently is set up just like the X-10 Powerhouse
+#remote. (with radio buttons instead of a dial for the house, and radio
+#buttons instead of a switch for chosing devices 1-8 or 9-16. 
+#
+#    (Version 0.1    Initial release)    (6/7/1999)
+#    Chris Kuster (ckuster@hrair.physics.ncsu.edu)
+#
+#    (Version 0.1.1  1st bugfix release) (6/8/1999)
+#    Chris Kuster (ckuster@hrair.physics.ncsu.edu)
+#    Changes since Version 0.1
+#     1) Dimmer functions actually exist
+#     2) There is now a variable to store the name of the bottle_rocket function
+#
+#  Things to do for future versions:
+#    1) A dial interface for the house selection (to be like the remote)
+#    2) A little light (like on the remote)
+#    3) A switch interface for the range selection (to be like the remote)
+#    4) A labeling scheme (so you can tell what's what)
+#    5) Better looking in general (may not be possible with tk/tcl)
+#    6) A more flexible method of calling the executable (paths not hard wired)
+#  Most of these are based on the assumption that it is better to have an
+#    interface like the remote.  This may not be a good assumption.
+#
+# (c) 1999 Chris Kuster (ckuster@hrair.physics.ncsu.edu).  
+#  Free Software.  GPL applies.
+#  No warranties expressed or implied.
+
+#  A procedure to turn on a given device in a given house
+proc fire_ignite {House Device Range BottleRocket} {
+  set currentDevice [expr {$Device + 8 * $Range}]
+  exec $BottleRocket $House$currentDevice on
+}
+
+#  A procedure to turn off a given device in a given house
+proc fire_dowse {House Device Range BottleRocket} {
+  set currentDevice [expr {$Device + 8 * $Range}]
+  exec $BottleRocket $House$currentDevice off
+}
+
+#  A procedure to dim the dimmer for a given house
+proc fire_dimmer {House BottleRocket} {
+  exec $BottleRocket $House dim
+}
+
+#  A procedure to brighten the dimmer for a given house
+proc fire_brighter {House BottleRocket} {
+  exec $BottleRocket $House bright
+}
+
+#  This procedure creates all of the widgets and associates them with the
+#    desired commands.
+proc fire_create_menu {} {
+
+# Create a method of choosing the letter of the house containing
+#   the desired device.
+  frame .house -borderwidth 2
+  pack .house -side left -fill y
+  
+# Title "House"
+  label .house.title -text "House"
+  pack .house.title
+
+# Radio buttons for each house (would really like a dial)
+  foreach h {"A" "B" "C" "D" "E" "F" "G" "H" "I" "J" "K" "L" "M" "N" "O" "P"} {
+    radiobutton .house.h$h -text $h -variable currentHouse -value $h
+  
+    pack .house.h$h 
+  } 
+
+# Titles "On" "Off"
+  frame .titles -borderwidth 2
+  pack .titles -side top -fill x
+  label .titles.on  -text "On"
+  label .titles.off -text "Off"
+  pack .titles.on  -side left
+  pack .titles.off -side right
+
+# The following lines create the on/off buttons for the devices.
+# I consider the fact that I wasn't able to accomplish this with
+#   a loop to be a personal failing.  If I used plain buttons
+#   and a loop, there were temporal scope problems with variables.
+#   I was able to create a working loop if I used radiobuttons, but
+#   that didn't look right at all. (Ob)
+
+# Device 1 on/off buttons
+  frame .a1 -borderwidth 2
+  pack .a1 -side top -fill x  
+  button .a1.on  -text "1" -command {fire_ignite $currentHouse 1 $currentRange $bottleRocket}
+  button .a1.off -text "1" -command {fire_dowse  $currentHouse 1 $currentRange $bottleRocket}
+  pack .a1.on .a1.off -side left
+
+# Device 2 on/off buttons
+  frame .a2 -borderwidth 2
+  pack .a2 -side top -fill x  
+  button .a2.on  -text "2" -command {fire_ignite $currentHouse 2 $currentRange $bottleRocket}
+  button .a2.off -text "2" -command {fire_dowse  $currentHouse 2 $currentRange $bottleRocket}
+  pack .a2.on .a2.off -side left
+
+# Device 3 on/off buttons
+  frame .a3 -borderwidth 2
+  pack .a3 -side top -fill x  
+  button .a3.on  -text "3" -command {fire_ignite $currentHouse 3 $currentRange $bottleRocket}
+  button .a3.off -text "3" -command {fire_dowse  $currentHouse 3 $currentRange $bottleRocket}
+  pack .a3.on .a3.off -side left
+
+# Device 4 on/off buttons
+  frame .a4 -borderwidth 2
+  pack .a4 -side top -fill x  
+  button .a4.on  -text "4" -command {fire_ignite $currentHouse 4 $currentRange $bottleRocket}
+  button .a4.off -text "4" -command {fire_dowse  $currentHouse 4 $currentRange $bottleRocket}
+  pack .a4.on .a4.off -side left
+
+# Device 5 on/off buttons
+  frame .a5 -borderwidth 2
+  pack .a5 -side top -fill x  
+  button .a5.on  -text "5" -command {fire_ignite $currentHouse 5 $currentRange $bottleRocket}
+  button .a5.off -text "5" -command {fire_dowse  $currentHouse 5 $currentRange $bottleRocket}
+  pack .a5.on .a5.off -side left
+
+# Device 6 on/off buttons
+  frame .a6 -borderwidth 2
+  pack .a6 -side top -fill x  
+  button .a6.on  -text "6" -command {fire_ignite $currentHouse 6 $currentRange $bottleRocket}
+  button .a6.off -text "6" -command {fire_dowse  $currentHouse 6 $currentRange $bottleRocket}
+  pack .a6.on .a6.off -side left
+
+# Device 7 on/off buttons
+  frame .a7 -borderwidth 2
+  pack .a7 -side top -fill x  
+  button .a7.on  -text "7" -command {fire_ignite $currentHouse 7 $currentRange $bottleRocket}
+  button .a7.off -text "7" -command {fire_dowse  $currentHouse 7 $currentRange $bottleRocket}
+  pack .a7.on .a7.off -side left
+
+# Device 8 on/off buttons
+  frame .a8 -borderwidth 2
+  pack .a8 -side top -fill x  
+  button .a8.on  -text "8" -command {fire_ignite $currentHouse 8 $currentRange $bottleRocket}
+  button .a8.off -text "8" -command {fire_dowse  $currentHouse 8 $currentRange $bottleRocket}
+  pack .a8.on .a8.off -side left
+
+# All right.  Now that that's over, we need buttons for the dimmer
+
+  frame .dimmer -borderwidth 2
+  pack .dimmer -side top -fill x  
+  button .dimmer.brite -text "+" -command {fire_brighter $currentHouse $bottleRocket}
+  button .dimmer.dim   -text "-" -command {fire_dimmer   $currentHouse $bottleRocket}
+  pack .dimmer.brite .dimmer.dim -side left
+
+# And finally, there needs to be a way to switch between devices 1-8 and 9-16
+
+  frame .rangeSwitch -borderwidth 5
+  pack .rangeSwitch -side top -fill x
+  radiobutton .rangeSwitch.low  -text "1-8"  -variable currentRange -value 0
+  radiobutton .rangeSwitch.high -text "9-16" -variable currentRange -value 1
+  pack .rangeSwitch.low .rangeSwitch.high -side top
+}
+
+#  A procedure to actually show the window
+proc fire_show_menu {} {
+  if {[winfo exists .fire]} {
+      wm deiconify .fire
+      raise .fire
+  }
+}
+
+
+# Actual start of the script.
+fire_create_menu
+fire_show_menu
